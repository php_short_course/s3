<?php 

// Objects as Variables

$buildingObj = (object) [
	'name' => 'Caswynn Building',
	'floors' => 8, 
	'address' => (object) [
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	] 
];

// // Objects from Classes
// class Building {
// 	// 
// 	public $name;
// 	public $floors; 
// 	public $address;

// 	// Constructor
// 	public function __construct($name, $floors, $address) {
// 		$this->name = $name;
// 		$this->floors = $floors; 
// 		$this->address = $address;
// 	}

// 	// methods
// 	public function printName() {
// 		return "The name of the building is $this->name";
// 	}
// }


// // Instaces
// $building = new Building('Casswyn Building', 8, "Timog Avenue, Quezon City, Philippines");

// // Inheritance and Polymorphism

// class Condominium extends Building {

// 	public function printName() {
// 		return "The name of the Condominium is $this->name";
// 	}
// }


// // Instance
// $Condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue', 'Makati City, Philippines');

// s03 Activity

class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->lastName.";
	}
};

$person = new Person('Senku', ' ', 'Ishigami');

class Developer extends Person {
	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}
};

$developer = new Developer('John', 'Finch', 'Smith');

class Engineer extends Person {
	public function printName(){
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
};

$engineer = new Engineer('Harold', 'Myers', 'Reese');

// s04 Activity

class Building {
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name= $name;
		$this->floors= $floors;
		$this->address= $address;
	}

	public function getName(){
		return $this->name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setName($name){
		$this->name = $name;
	}
};

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

class Condominium extends Building {

	public function getName(){
		return $this->name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setName($name){
		$this->name = $name;
	}
};

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');

?>
