<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S3: Classes and Objects, Inheritance, and Polymorphism</title>
</head>
<body>

	<h1>Objects from Variable</h1>

	<p><?php echo $buildingObj->name; ?></p>


	<h1>Objects from Classes</h1>
	<?php var_dump($building); ?>
	<p><?php print_r($building->printName()); ?></p>


		<h1>Inheritance and Polymorphism</h1> -->

	<!-- method of building -->

<!-- 	<p><//?php echo $building->printName(); ?></p>

	<p><//?php echo $condominium->printName(); ?></p>

	<h1>Access Modifiers</h1> -->

	<!-- <h2>Building Variables</h2>

	<p><//?php echo $building->name; ?></p>

	<h2>Condominium Variables</h2>

	<p><//?php echo $condominium->name; ?></p> -->

<!-- 	<h1>Encapsulation</h1>

	<p>The name of the condominium is <//?php echo $condominium->getName(); ?></p>

	<p><//?php $condominium->setName('Enzo Tower') ?></p>

	<p>The name of the condo has been changed to <//?php echo $condominium->getName(); ?></p> -->

	<!-- s03 Activity -->

	<h1>Person</h1>

	<p><?php echo $person->printName(); ?></p>

	<h1>Developer</h1>

	<p><?php echo $developer->printName(); ?></p>

	<h1>Engineer</h1>

	<p><?php echo $engineer->printName(); ?></p>

	<!-- s04 Activity -->

	<h1>Building</h1>

	<p>The name of the building is <?php echo $building->getName(); ?>.</p>
	<p>The <?php echo $building->getName(); ?> has <?php echo $building->getFloors(); ?> floors.</p>
	<p>The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?>.</p>

	<?php $building->setName('Caswynn Complex'); ?>
	<p>The name of the building has been changed to <?php echo $building->getName(); ?>.</p>

	<h1>Condominium</h1>

	<p>The name of the condominium is <?php echo $condominium->getName(); ?>.</p>
	<p>The <?php echo $condominium->getName(); ?> has <?php echo $condominium->getFloors(); ?> floors.</p>
	<p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?>.</p>

	<?php $condominium->setName('Enzo Tower'); ?>
	<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?>.</p>

</body>
</html>



	



</body>
</html>